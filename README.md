Import maps from Kanka.io to Foundry

# Settings

In order for the module to work, a number of settings must be configured by going to Game Settings > Configure Settings > Module Settings and finding the section called Kanka Importer. The settings are explained below:

* **Scene compendium**: The compendium to import your Kanka maps into. This compendium is deleted and re-populated on every restart, so it's advised to create a new compendium just for Kanka maps.
* **Journal compendium**: The compendium to import your Kanka locations into. This compendium is deleted and re-populated on every restart, so it's advised to create a new compendium just for Kanka locations.

* **Campaign ID**: The ID of the Kanka campaign to import into this world. The Campaign ID may be found by visting your campaign's landing page and looking at the URL, which should be something like https://kanka.io/en-US/campaign/XXXXX, where XXXXX is your Campaign ID

* **Kanka API Key**: You can generate a new Kanka API key by going to https://kanka.io/en-US/settings/api and clicking "Create New Token". Pick a memorable name for your token (e.g. "FoundryVTT"), and copy/paste the token into this field.

# How to use

Once all the settings are configured correctly, the module is straightforward. As soon as a GM loads into the game, it will automatically import your Locations from Kanka into two compendiums, one Scene compendium containing the maps, and one Journal compendium containing the location text. When you import a Scene, it will automatically import the corresponding Journal and link them together once you close the Scene dialog. *It will look like the Journal is not set in the Scene dialog. This is a bug. Once you scroll down and click "Save Changes", the journal entry will be linked automatically.*