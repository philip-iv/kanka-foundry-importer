# 0.2.1

* Fix manifest URLs

# 0.2.0

* Use new Kanka image server

* When a Scene is imported from the compendium, automatically import & link the corresponding Journal entry

* Only run importer for GM

# 0.1.0

* Initial release