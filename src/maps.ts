import { getLocations, Location } from './kanka';

const DEFAULT_IMG = "https://kanka.io/images/defaults/locations.jpg";
const DEFAULT_THUMB = "https://kanka.io/images/defaults/locations_thumb.jpg";


const getSize = async (src: string): Promise<Array<Number>> => {
    return new Promise((resolve, reject) => {
        let img = new Image();
        img.onload = () => resolve([img.height, img.width]);
        img.onerror = reject;
        img.src = src;
    })
}

export async function importMaps(sceneCompendium: Compendium<Scene>, journalCompendium: Compendium<JournalEntry>) {
    const locations = await getLocations();
    const maps = locations.filter(l => l.map !== DEFAULT_THUMB);
    
    await createScenes(maps, sceneCompendium);

    if (journalCompendium) {
        await createJournalEntries(locations, journalCompendium);

        // Import the journal entry before the scene is created, if it hasn't been imported already
        Hooks.on("preCreateScene", async (scene, data, id) => {
            if (!scene.img.includes("kanka") || data.temporary == true || game.journal.find((j: JournalEntry) => j.name == scene.name) != null)
                return;
            const index = await journalCompendium.getIndex();
            const entryId = index.find(i => i.name == scene.name)._id;
            game.journal.importFromCollection(`${journalCompendium.metadata.package}.${journalCompendium.metadata.name}`, entryId);
        });

        // Associate the journal entry to the scene
        Hooks.on("preUpdateScene", (scene, data, idk, id) => {
            if (!scene.img.includes("kanka") || data.temporary == true)
                return;
            const entry = game.journal.find((e: JournalEntry) => e.name == scene.name);
            data.journal = entry.id;
        });
    }
}

async function createScenes(locations: Array<Location>, sceneCompendium: Compendium<Scene>) {    
    // Delete all scenes
    sceneCompendium.getIndex().then(i => i.forEach(id => sceneCompendium.deleteEntity(id._id)));

    let scenes = locations.map(async l => {
        const imgUrl = l.map;
        const size = await getSize(imgUrl);
        return {
            name: l.name,
            // Hacky workaround, should try and properly download maps
            img: imgUrl,
            type: 'Scene',
            flags: [],
            height: size[0],
            width: size[1],
        };
    });

    for (let map of scenes) {
        const scene: Scene = await Scene.create(await map, {temporary: true});
        await sceneCompendium.importEntity(scene);
    }
}

async function createJournalEntries(locations: Array<Location>, journalCompendium: Compendium<JournalEntry>) {
    // Delete journals
    journalCompendium.getIndex().then(i => i.forEach(id => journalCompendium.deleteEntity(id._id)));
    
    const journalEntries = locations.map(l => {
        return {
            name: l.name,
            content: l.entry,
            img: l.image_full != DEFAULT_IMG ? l.image_full : l.map,
            type: 'JournalEntry',
            flags: [],
        }
    });

    for (let entry of journalEntries) {
        const journalEntry: JournalEntry = <JournalEntry> await JournalEntry.create(entry, {temporary: true});
        await journalCompendium.importEntity(journalEntry);
    }
}

