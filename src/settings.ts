import module from './module.json';

export default function () {
    const sceneCompendiums = game.packs
      .filter((pack) => pack.entity === "Scene" && !pack.locked)
      .reduce((choices, pack) => {
        choices[pack.collection] = pack.metadata.label;
        return choices;
      }, {});
  
    const journalCompendiums = game.packs
      .filter((pack) => pack.entity === "JournalEntry" && !pack.locked)
      .reduce((choices, pack) => {
        choices[pack.collection] = pack.metadata.label;
        return choices;
      }, {});
    
    registerSetting("scene-compendium", {
      scope: "world",
      config: true,
      type: String,
      isSelect: true,
      choices: sceneCompendiums,
    });
  
    registerSetting("journal-compendium", {
      scope: "world",
      config: true,
      type: String,
      isSelect: true,
      choices: journalCompendiums,
    });

    registerSetting("kanka-campaign", {
      scope: "world",
      config: true,
      type: String
    });

    registerSetting("kanka-api-key", {
      scope: "client",
      config: true,
      type: String
    });
  }

function registerSetting(key: string, data: any) {
  data.name = `${module.name}.${key}.name`;
  data.hint = `${module.name}.${key}.hint`;
  game.settings.register(module.name, key, data);
}