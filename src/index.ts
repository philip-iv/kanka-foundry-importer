import settings from './settings';
import { importMaps } from './maps';
import module from './module.json';

Hooks.once("ready", async function() {
    if (!game.user.isGM)
        return;

    settings();

    const sceneCompendiumName: string = game.settings.get(module.name, "scene-compendium");
    let sceneCompendium: Compendium<Scene> = game.packs.find(p => p.collection === sceneCompendiumName);
    if (sceneCompendium && sceneCompendium.locked) {
        console.error(`Cannot add scenes to locked compendium ${sceneCompendiumName}`);
        sceneCompendium = null;
    }

    const journalCompendiumName: string = game.settings.get(module.name, "journal-compendium");
    let journalCompendium: Compendium<JournalEntry> = game.packs.find(p => p.collection === journalCompendiumName);
    if (journalCompendium && journalCompendium.locked) {
        console.error(`Cannot add journal entries to locked compendium ${journalCompendiumName}`);
        journalCompendium = null;
    }

    if (sceneCompendium && journalCompendium)
        await importMaps(sceneCompendium, journalCompendium);
    else
        console.error('You must set a scene & journal compendium before the importer can function.');
});