import axios, { AxiosInstance } from 'axios';
import module from './module.json';

export type Location = {
    id: Number,
    name: string,
    entry: string,
    image: string,
    image_full: string,
    image_thumb: string,
    is_private: boolean,
    map: string
}

type LocationList = {
    data: Array<Location>,
    links: {
        next: string
    }
};

let apiClient: AxiosInstance = null;

function getApiClient() {
    if (!apiClient) {
        apiClient = axios.create({
            baseURL: 'https://kanka.io/api/1.0/campaigns/' + game.settings.get(module.name, "kanka-campaign"),
            responseType: 'json',
            headers: {
                Authorization: 'Bearer ' + game.settings.get(module.name, "kanka-api-key"),
                'Content-Type': 'application/json'
            }
        });
    }
    return apiClient;
}

const getLocations = async () => {
    try {
        let response = await getApiClient().get<LocationList>('/locations');
        let locations = response.data.data;
        while (response.data.links.next) {
            response = await getApiClient().get<LocationList>(response.data.links.next);
            locations = locations.concat(response.data.data);
        }
        console.log("Kanka | " + locations.length.toString())
        return locations;
    } catch (err) {
        console.error(err);
    }
};

const getLocation = async (id: Number) => {
    try {
        const response = await getApiClient().get<Location>('/locations/' + id.toString());
        return response.data;
    } catch (err) {
        console.error(err);
    }
}

export {getLocation, getLocations};