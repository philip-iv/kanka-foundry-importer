const path = require('path');
const package = require('./package.json');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const modulePlugin = new CopyWebpackPlugin([{
	from: "./src/module.json",
	to:   "./module.json",
	transform (content, path) {
		// copy-webpack-plugin passes a buffer
		let manifest = JSON.parse(content.toString());

		// Set values from package
		manifest.version = package.version;
		manifest.author = package.author;
		manifest.description = package.description;
		manifest.url = package.homepage;
		manifest.download = `https://gitlab.com/philip-iv/kanka-foundry-importer/-/raw/${package.version}/release/release.zip`
		manifest.bugs = package.bugs.url;
		
		// Convert back to a String
		return JSON.stringify(manifest, null, 4);
}}]);


module.exports = {
	devtool: 'inline-source-map',
    module: {
		rules: [
			{
			test: /\.ts$/,
			use: 'ts-loader',
			exclude: /node_modules/,
			},
		],
	},
    resolve: {
      	extensions: [ '.ts', '.js' ],
    },
    output: {
      	filename: 'bundle.js',
      	path: path.resolve(__dirname, 'dist'),
    },
   plugins: [ modulePlugin ],
   target: "electron-main"
}